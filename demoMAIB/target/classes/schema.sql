DROP TABLE IF EXISTS deposit_account;
CREATE TABLE deposit_account (id INT PRIMARY KEY, deposit_number VARCHAR(55), balance money, currency VARCHAR(55),last_time_operation datetime2 );