import java.sql.*;
import java.util.*;
import java.util.logging.Logger;

public class DemoApplication {

    private static final Logger log;
    private static Scanner scanner = new Scanner(System.in);
    private static Connection connection;
    private static Statement statement;

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%4$-7s] %5$s %n");
        log = Logger.getLogger(DemoApplication.class.getName());
    }

    public static void main(String[] args) throws Exception {
        log.info("Loading application properties");
        Properties properties = new Properties();
        properties.load(DemoApplication.class.getClassLoader().getResourceAsStream("application.properties"));
        log.info("Connecting to the database");
        connection = DriverManager.getConnection(properties.getProperty("url"), properties);
        log.info("Database connection test: " + connection.getCatalog());

        log.info("Create database schema");
        scanner = new Scanner(Objects.requireNonNull(DemoApplication.class.getClassLoader().getResourceAsStream("schema.sql")));
        executeSQLFile();
        log.info("Create procedure");
        scanner = new Scanner(Objects.requireNonNull(DemoApplication.class.getClassLoader().getResourceAsStream("procedure.sql")));
        executeSQLFile();

        insertData();
        insertData();
        insertData();
        executeProcedure();
        log.info("Closing database connection");
        connection.close();
        scanner.close();
    }

    private static void insertData() throws SQLException {
        long randomId = (long) (Math.random() * 100);
        long timeInMillis = System.currentTimeMillis();
        DepositAccount depositAccount = new DepositAccount(randomId, "007", 79.20, "USD", new Timestamp(timeInMillis));
        log.info("Insert data");
        PreparedStatement insertStatement = connection
                .prepareStatement("INSERT INTO deposit_account (id, deposit_number, balance, currency, last_time_operation) " +
                        "VALUES (?, ?, ?, ?, ?);");
        insertStatement.setLong(1, depositAccount.getId());
        insertStatement.setString(2, depositAccount.getDepositNumber());
        insertStatement.setDouble(3, depositAccount.getBalance());
        insertStatement.setString(4, depositAccount.getCurrency());
        insertStatement.setTimestamp(5, depositAccount.getLastTimeOperation());
        insertStatement.executeUpdate();
    }

    public static void executeSQLFile() throws SQLException {
        statement = connection.createStatement();
        while (scanner.hasNextLine()) {
            statement.execute(scanner.nextLine());
        }
    }

    public static void executeProcedure() throws SQLException {
        statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("{call dbo.getDeposits}");
        while (rs.next()) {
            displayRow(rs);
        }
    }

    private static void displayRow(ResultSet rs) throws SQLException {
        System.out.println(rs.getInt(1) + " , " +                 // SQL integer type.
                rs.getString(2) + " , " +                         // SQL varchar type.
                rs.getDouble(3) + " , " +                         // SQL decimal type.
                rs.getString(4) + " , " +                         // SQL varchar type.
                rs.getTimestamp(5));                              // SQL datetime type.
        System.out.println();
    }
}