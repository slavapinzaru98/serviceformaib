import java.sql.Timestamp;

public class DepositAccount {

    private Long id;
    private String depositNumber;
    private Double balance;
    private String currency;
    private Timestamp lastTimeOperation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Timestamp getLastTimeOperation() {
        return lastTimeOperation;
    }

    public void setLastTimeOperation(Timestamp lastTimeOperation) {
        this.lastTimeOperation = lastTimeOperation;
    }

    public DepositAccount() {
    }

    public DepositAccount(Long id, String depositNumber, Double balance, String currency, Timestamp lastTimeOperation) {
        this.id = id;
        this.depositNumber = depositNumber;
        this.balance = balance;
        this.currency = currency;
        this.lastTimeOperation = lastTimeOperation;
    }

    @Override
    public String toString() {
        return "DepositAccount{" +
                "id=" + id +
                ", depositNumber='" + depositNumber + '\'' +
                ", balance=" + balance +
                ", currency='" + currency + '\'' +
                ", lastTimeOperation=" + lastTimeOperation +
                '}';
    }
}
